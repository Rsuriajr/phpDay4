<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE-Edge">

	<title> For Each Loops and Arrays </title>


</head>
<body>

	<?php 
		//index array
		$students = array("Vheil","James","Reah");
		$arrcntr = count($students);

		sort($students); //sorts array in ascending (alphabetically if letters)
		//rsort($students) //sorts array in descending order

		//echo "$students[1]<br>"; //display a specific index

		//echo count($students)."<br>";   //to return the length of the array

		for ($x=0; $x < $arrcntr; $x++) {  //for loop to display all the elements in an array
			echo "$students[$x]<br>";	
		}

		//Assiociative Arrays are arrays that use named keys that you assign to them
		//=> is the separator for associative arrays example "NamedKey" => "value"
		//in the case of foreach loops, => assigns the key to its equivalent value in the array


		$age = [
		//key => value
		"Peter" => "35",
		"Ben" => "37",
		"Joe" => "43"
		];

		foreach ($age as $name => $value) {
			echo "key =". $name . ", Value =" . $value . "<br>";
		}

		$letters = ['H', 'E', 'L', 'L', 'O'];

		foreach ($letters as $letter) {
			echo $letter."<br>";
		}


		$pokemon = [
			'Ash' => "21",
			'Misty' => "22",
			'Brock' => "23"
		];

		//asort($pokemon); // sorts associate arrays in ascending order according to its value.
		//ksort($pokemon); // sorts associate array keys from a-z
		//arsort($pokemon); //sorts associate arrays according to its value
		krsort($pokemon); // sorts associate array keys from z-a
		foreach ($pokemon as $character => $age) {
			echo "The age of " . $character . " is " . $age . "<br>";
		}


		//multi dimentional arrays are arrays within an array

		$team_ironman = ["Tony", "War machine", "Vision"];
		$team_cap = ["Cap America", "Bucky", "Hawkeye", "Falcon"];
		$civil_war = [$team_ironman, $team_cap];


		echo $civil_war[0][0]. "<br>";//output Tony
		echo $civil_war[1][2]. "<br>";//output Hawkeye
		echo $civil_war[1][1]. "<br>";//output Bucky

		//Activity 
		$items = [

		['product' => "Lenovo Laptop", 'price' => 600.00, 'quantity' => 100],
		['product' => "ASUS Tablet", 'price' => 100.00, 'quantity' => 10],
		['product' => "Acer All-in-one", 'price' => 300.00, 'quantity' => 300],
		['product' => "Hp Laptop", 'price' => 400.00, 'quantity' => 1],
		['product' => "Dell Laptop", 'price' => 350.00, 'quantity' => 20]

		];

		echo "<h1> Display Product</h1>";
		//Activity 1
		foreach ($items as $produkto) {
			echo "<li>" . $produkto['product'] . "</li>";
		}

		//Activity 2
		echo "<h1> Display Price</h1>";
		foreach ($items as $prices) {
			echo "<li>" . $prices['price'] . "</li>";
		}

		echo "<h1> Display Quantity</h1>";
		//Activity 3 

		foreach ($items as $quantities) {
			echo "<li>" . $quantities['quantity'] . "</li>";
		}

		echo "<h1> Display Chart</h1>";
		//Activity 4

		foreach ($items as $mine) {
			echo "<table><tr>
			<th>Products</th>
			<th>Prices</th>
			<th>Quantity</th></tr>";
			echo $mine['product'];
			echo $mine['price'];
			echo $mine['quantity'] . "<br>";
		}

	 ?>


</body>
</html>